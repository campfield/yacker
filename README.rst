A YAML wrapper for Packer
=========================

.. image:: https://gitlab.com/matilda.peak/yacker/badges/master/pipeline.svg
   :target: https://gitlab.com/matilda.peak/yacker
   :alt: Pipeline Status (yacker)

.. image:: https://badge.fury.io/py/matildapeak-yacker.svg
   :target: https://badge.fury.io/py/matildapeak-yacker
   :alt: PyPI package (latest)

.. image:: https://readthedocs.org/projects/yacker/badge/?version=latest
   :target: https://yacker.readthedocs.io/en/latest/?badge=latest
   :alt: Documentation Status (yacker)

``yacker`` is Python utility that wraps `Packer`_ with the ability to define
template and variable files in YAML.

Sadly, Packer (at the moment) only supports template definitions in JSON,
which (for me) isn't a particularly human-friendly file format (I like to add
comments and break lines to make them easier to read). So, here's a utility
that allows you to define your files using a much more friendly format.

.. _Packer: https://www.packer.io

Installation
------------

``yacker`` is published on `PyPI`_ and can be installed from there::

    pip install matildapeak-yacker

.. _PyPI: https://pypi.org/project/matildapeak-yacker

Documentation
-------------

Yacker documentation is available on `Read The Docs`_.

.. _Read The Docs: https://yacker.readthedocs.io/en/latest/

Get in touch
------------

- Report bugs, suggest features or view the source code `on GitLab`_.

.. _on GitLab: https://gitlab.com/matilda.peak/yacker.git
